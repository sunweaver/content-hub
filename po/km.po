# Khmer translation for content-hub
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd 2014
# This file is distributed under the same license as the content-hub package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: content-hub\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2014-08-14 12:39+0100\n"
"PO-Revision-Date: 2014-10-08 07:40+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Khmer <km@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2016-12-01 04:56+0000\n"
"X-Generator: Launchpad (build 18282)\n"

#: import/Lomiri/Content/ContentPeerPicker10.qml:48
#: import/Lomiri/Content/ContentPeerPicker11.qml:49
msgid "Choose from"
msgstr "ជ្រើស​ពី"

#: import/Lomiri/Content/ContentPeerPicker10.qml:48
#: import/Lomiri/Content/ContentPeerPicker11.qml:49
msgid "Open with"
msgstr "បើកជាមួយ"

#: import/Lomiri/Content/ContentPeerPicker10.qml:48
#: import/Lomiri/Content/ContentPeerPicker11.qml:49
msgid "Share to"
msgstr "ចែករំលែកទៅ"

#: import/Lomiri/Content/ContentPeerPicker10.qml:158
#: import/Lomiri/Content/ContentPeerPicker11.qml:175
msgid "Apps"
msgstr "កម្មវិធី"

#: import/Lomiri/Content/ContentPeerPicker10.qml:194
#: import/Lomiri/Content/ContentPeerPicker11.qml:211
msgid ""
"Sorry, there aren't currently any apps installed that can provide this type "
"of content."
msgstr ""
"សូមទោស "
"មិនមានកម្មវិធីណាមួយដែលបានដំឡើងនៅពេលបច្ចុប្បន្ននេះដែលអាចផ្តល់នូវប្រភេទមាតិកានេ"
"ះបាន ។"

#: import/Lomiri/Content/ContentPeerPicker10.qml:194
#: import/Lomiri/Content/ContentPeerPicker11.qml:211
msgid ""
"Sorry, there aren't currently any apps installed that can handle this type "
"of content."
msgstr ""
"សូមទោស "
"មិនមានកម្មវិធីណាមួយដែលបានដំឡើងនៅពេលបច្ចុប្បន្ននេះដែលអាចគ្រប់គ្រងប្រភេទមាតិកាន"
"េះបាន ។"

#: import/Lomiri/Content/ContentPeerPicker10.qml:210
#: import/Lomiri/Content/ContentPeerPicker11.qml:227
msgid "Devices"
msgstr "ឧបករណ៍​"

#: import/Lomiri/Content/ContentPeerPicker10.qml:247
#: import/Lomiri/Content/ContentPeerPicker11.qml:56
#: import/Lomiri/Content/ContentTransferHint.qml:65
msgid "Cancel"
msgstr "បោះបង់"

#: import/Lomiri/Content/ContentTransferHint.qml:52
msgid "Transfer in progress"
msgstr "កំពុងផ្ទេរប្រាក់"

#: src/com/lomiri/content/detail/service.cpp:216
msgid "Download Complete"
msgstr "ទំនាញយក បានជោគជ័យ"

#: src/com/lomiri/content/detail/service.cpp:234
msgid "Open"
msgstr "បើក"

#: src/com/lomiri/content/detail/service.cpp:241
msgid "Dismiss"
msgstr "បណ្ដេញ​ចេញ"

#: src/com/lomiri/content/detail/service.cpp:259
msgid "Download Failed"
msgstr "ទំនាញយក បានបរាជ័យ"
